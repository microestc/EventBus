﻿using System;

namespace Microestc.EventBus.RabbitMQ
{
    public interface IEventBus
    {
        void Dispose();
        void Publish(IntegrationEvent @event);
        void Subscribe(Type eventType, Type handlerType);
        void Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>;
        void SubscribeDynamic<TH>(string eventName) where TH : IDynamicIntegrationEventHandler;
        void Unsubscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>;
        void UnsubscribeDynamic<TH>(string eventName) where TH : IDynamicIntegrationEventHandler;
    }
}