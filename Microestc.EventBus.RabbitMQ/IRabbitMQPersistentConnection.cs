﻿using RabbitMQ.Client;

namespace Microestc.EventBus.RabbitMQ
{
    public interface IRabbitMQPersistentConnection
    {
        bool IsConnected { get; }

        IModel CreateModel();
        void Dispose();
        bool TryConnect();
    }
}