﻿namespace Microestc.EventBus.RabbitMQ
{
    public class EventBusSettings
    {
        public string Connection { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Exchange { get; set; }

        public string Queue { get; set; }

        public int RetryCount { get; set; }
    }
}