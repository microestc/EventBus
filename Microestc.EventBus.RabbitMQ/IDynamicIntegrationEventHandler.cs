﻿using System.Threading.Tasks;

namespace Microestc.EventBus.RabbitMQ
{
    public interface IDynamicIntegrationEventHandler
    {
        Task Handle(dynamic eventData);
    }
}
