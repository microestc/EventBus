﻿using System;

namespace Microestc.EventBus.RabbitMQ
{
    public class IntegrationEvent
    {
        public IntegrationEvent()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }

        public virtual Guid Id { get; }

        public virtual DateTime CreationDate { get; }

    }
}
